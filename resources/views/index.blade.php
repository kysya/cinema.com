@extends('welcome')
@section('content')

@foreach ($movies as $movie)
<div class="card" style="width: 18rem;">
  <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
  <div class="card-body">
    <h5 class="card-title">{{$movie->name}}</h5>
    <p class="card-text">{{$movie->desctiption}}</p>
    <a href="#" class="btn btn-primary">{{$movie->duration}}</a>
    <a href="{{ route('afishas.edit', $movie->id ) }}" class="btn btn-primary"><button">Edit</button></a>
  </div>
</div>
@endforeach
@endsection