<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Movies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name')->nullable();
			$table->text('desctiption')->nullable();
			$table->date('date')->nullable();
			$table->string('genre')->nullable();
			$table->string('duration')->nullable();
			$table->timestamps();
			$table->integer('genre_id')->unsigned()->nullable();
			$table->foreign('genre_id')->references('id')->on('genres');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
