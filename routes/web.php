<?php

use Illuminate\Support\Facades\Route;
use app\Http\Controllers\AfishaController;
use App\Http\Models\Movie;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'App\\Http\\Controllers'], function() {
    Route::resource('movies', "AfishaController")->names('afishas');
});
Route::get('store', function() {
    return view('store');
});
// Route::get('afisha/show/{movie}', function () {
//     return view('show');
//     })->name('show-form');